package gol.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import gol.base.util.CycleChecker;
import gol.base.util.LiveCellCycleChecker;

public class FullGameGrid implements GameGrid {

	private static final char VALUES[] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	
	private int sizeX, sizeY;
	private List <GameCell> allCells = null;
	private boolean cycled = false;
	private CycleChecker cc = null;
	
	@Override
	public void initialize(int x, int y) {
		sizeX = x;
		sizeY = y;
		cycled = false;
		
		allCells = new ArrayList<>();
		for (int i = 0; i< sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				allCells.add(new GameCell(j,i,false));
			}
		}

		cc = new LiveCellCycleChecker();
	}

	@Override
	public void tick() {
		List<GameCell> nextGen = new ArrayList<>();
		
		for (GameCell cell : allCells) {
			nextGen.add( new GameCell(cell.x,cell.y,tickCell(cell)) );
		}

		allCells = nextGen;
		cycled = cc.check(this);

		
	}

	private boolean tickCell(GameCell cell) {
		/*
		 *  Any live cell with fewer than two live neighbors dies, as if by underpopulation.
		 *	Any live cell with two or three live neighbors lives on to the next generation.
		 *	Any live cell with more than three live neighbors dies, as if by overpopulation.
		 *	Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
		 */
		
		 // figure out scan dimensions
		 // count all living
		int ii = (cell.x - 1 > 0) ? cell.x - 1 : 0;
		int ij = (cell.y - 1 > 0) ? cell.y - 1 : 0;
		int ei = (cell.x + 1 < sizeX) ? cell.x + 1 : sizeX-1;
		int ej = (cell.y + 1 < sizeY) ? cell.y + 1 : sizeY-1;
		
		int adjcentLive = 0;
		
		for (int i = ii; i <= ei; i++) {
			for (int j = ij; j <= ej; j++) {
				if (allCells.get(i + (sizeX * j)).isLiving() 
						&& (cell.x != i || cell.y != j)) {
					adjcentLive++;
				}
			}
		}
		if (adjcentLive == 3) {
			return true;			
		}
		else if (adjcentLive < 2) {
			return false;
		}
		else if (adjcentLive > 3) {
			return false;
		}
		else {
			return cell.isLiving();
		}
	}

	@Override
	public void print() {
		
		for (int j = 0; j < sizeY; j++) {
			for (int i = 0; i < sizeX; i++) {
				if (allCells.get(i + (sizeX * j)).isLiving()) {
					System.out.print("x");
				}
				else {
					System.out.print("O");
				}
			}
			System.out.println("");
		}
		
	}

	@Override
	public void seed(List<GameCell> liveNodes) {
		//
		
		for (Pair<Integer> p : liveNodes) {
			allCells.get(p.x + (sizeX * p.y)).setLiving(true);
		}
		
	}

	private String getStateCode() {
		StringBuilder sb = new StringBuilder();
		
		int place = 0;
		int rValue = 0;
		
		for (int i = 0; i < this.sizeX * this.sizeY; i++) {
			int val = i % 4;
			if (val == 0) {
				if (i > 0) {
					sb.append(FullGameGrid.VALUES[rValue]);
				}
				place = 0;
				rValue = 0;
				
			}
			rValue = rValue + (this.allCells.get(i).isLiving() ? 2^place : 0 );
			place++;
			
		}
		
		return sb.toString();
	}

	@Override
	public boolean hasCycled() {
		
		return cycled;
	}

	@Override
	public Collection<GameCell> getBoardCells() {
		
		return Collections.unmodifiableCollection(allCells);
	}

	@Override
	public Pair<Integer> getDimensions() {
		
		return new Pair<Integer>(this.sizeX,this.sizeY);
	}



}
