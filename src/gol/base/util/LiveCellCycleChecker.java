package gol.base.util;

import java.util.HashSet;
import java.util.Set;

import gol.base.GameCell;
import gol.base.GameGrid;

public class LiveCellCycleChecker implements CycleChecker{

	HashSet<Set<GameCell>> store = new HashSet<>();

	@Override
	public boolean check(GameGrid g) {
		
		boolean found = false;
		
		HashSet<GameCell> current = new HashSet<>();
		for (GameCell gc : g.getBoardCells()) {
			if (gc.isLiving()) {
				current.add(gc);
			}
		}
		if (store.contains(current)) {
			found = true;
		}
		else {
			store.add(current);
		}
		
		return found;
	}
}
