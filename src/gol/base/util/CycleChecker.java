package gol.base.util;

import gol.base.GameGrid;

public interface CycleChecker {

	boolean check(GameGrid g);
}
