package gol.base;

import java.util.Collection;
import java.util.List;

public interface GameGrid {
	
	/**
	 * Initialize the game grid to the specified size 
	 * @param x width
	 * @param y height
	 */
	void initialize(int x, int y);
	
	/**
	 * Execute one cycle on all cells
	 */
	void tick();
	
	/**
	 * Print the current state of the game board 
	 * not valid before initialized
	 */
	void print();
	
	void seed(List<GameCell> liveNodes);
	
	/**
	 * 
	 * @return true if the current state and the initial state are the same
	 */
	boolean hasCycled();
	
	/**
	 * 
	 * @return the current board information
	 */
	
	Collection<GameCell> getBoardCells();
	
	/**
	 * 
	 * @return a Pair containing the dimensions of the board
	 */
	Pair<Integer> getDimensions();

	
	
	
	
	
}
