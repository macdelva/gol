package gol.base;

import java.util.Objects;

public class Pair<X>  {

	public final X x;
	public final X y;
	
	public Pair(X x, X y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if (o == null || !(o instanceof Pair<?>)) {
			return false;
		}		
		else {
			Pair<?> op = (Pair<?>) o;
			return (op.x.equals(x) && op.y.equals(y));
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(x,y);
	}
	
}
