package gol.base;

/**
 * Describes one cell of the game
 */

public class GameCell extends Pair<Integer> {


	private boolean living = false;

	
	public GameCell(Integer x, Integer y) {
		super(x,y);
		living = true;

	}
	
	public GameCell(int x, int y, boolean isLiving) {
		super(x,y);
		living = isLiving;
	}
	
	public boolean isLiving() {
		return living;
	}

	public void setLiving(boolean living) {
		this.living = living;
	}
	
	public String toString() {
		
		return x +","+ y + ":"+living;
	}
	
	public Integer toInteger(int colWidth) {
		return null;
	}
	

}
