package gol.base;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gol.base.util.CycleChecker;
import gol.base.util.LiveCellCycleChecker;

public class SparseGameGrid implements GameGrid {

	private int sizeX, sizeY;
	private Set <GameCell> liveCells = null;
	private CycleChecker cc = new LiveCellCycleChecker();
	private boolean cycled;
	
	@Override
	public void initialize(int x, int y) {
		sizeX = x;
		sizeY = y;
		
		liveCells = new HashSet<>();
	}

	@Override
	public void tick() {
		
		if (liveCells == null || liveCells.isEmpty()) {
			return;
		}
		
		Map<GameCell, Integer> neighborCount = new HashMap<>();
		Set<GameCell> newLiving = new HashSet<>();
		
		for (GameCell p : liveCells) {
			//generate the scan around p
			int ii = (p.x - 1 > 0) ? p.x - 1 : 0;
			int ij = (p.y - 1 > 0) ? p.y - 1 : 0;
			int ei = (p.x + 1 < sizeX) ? p.x + 1 : sizeX-1;
			int ej = (p.y + 1 < sizeY) ? p.y + 1 : sizeY-1;
			
			for (int i = ii; i <= ei; i++) {
				for (int j = ij; j <= ej; j++) {
					if (i != p.x || j != p.y) {
						// for the cell in question
						GameCell np = new GameCell(i,j);
						if (!neighborCount.containsKey(np)) {
							neighborCount.put(np, 1);
						}
						else {
							neighborCount.put(np, neighborCount.get(np) + 1);
						}
					}
				}
			}
		}
		// turn mapset into living set
		for (GameCell p : neighborCount.keySet()) {
			int nCount = neighborCount.get(p);
			
			if (nCount == 3 || (nCount == 2 && liveCells.contains(p))) {
				newLiving.add(p);			
			}
		}
		
		liveCells = newLiving;
		cycled = cc.check(this);
		
	}

	@Override
	public void print() {
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				if (liveCells.contains(new Pair<Integer>(i,j))) {
					System.out.print("x");
				}
				else {
					System.out.print("O");
				}
			}
			System.out.println("");
		}
		
		
	}

	@Override
	public void seed( List<GameCell> liveNodes) {
		
		liveCells.addAll(liveNodes);
		
	}

	@Override
	public boolean hasCycled() {

		return cycled;
	}

	@Override
	public Collection<GameCell> getBoardCells() {
		
		return Collections.unmodifiableCollection(liveCells);
	}

	@Override
	public Pair<Integer> getDimensions() {
		
		return new Pair<Integer>(this.sizeX,this.sizeY);
	}
}
