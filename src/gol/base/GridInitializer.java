package gol.base;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class GridInitializer  {
	
	public GameGrid createGrid(String filename, GameGrid grid) throws IOException {
		
		if (filename == null || filename.isEmpty() || grid == null) {
			return null;
		}
		
		/*grid init file has 3 parts
		*1.) grid dimensions comma separated ints
		*2.) starting location of live cells comma separated ints
		*3.) x/o pattern of cells (within a pattern no cells can be skipped)
		* Also note that parts 1 & 2 are on the same line so first line has 4 ints
		*/
		
		
		List<String> lines = Files.readAllLines(Paths.get(filename,""));
		
		/* get dimensions */
		List<Integer> pos = new ArrayList<>();
		for (String sInt : lines.get(0).split(",")) {
			pos.add(Integer.parseInt(sInt));
		}
		
		/* read x/o */
		int px = pos.get(2);
		int cx; /* starting x,y positions for live cells */
		int cy = pos.get(3);

		List<GameCell> cells = new ArrayList<>();
		
		for (String line : lines) {
			cx = px;
			for (char ch : line.toCharArray()) {
				if (ch == 'x' || ch == 'X') {
					cells.add(new GameCell(cx,cy));
				}
				cx++;
			}
			cy++;
		}
		
		grid.initialize(pos.get(0), pos.get(1));
		grid.seed(cells);
		
		return grid;
	}
	

}
