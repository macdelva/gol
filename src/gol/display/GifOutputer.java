package gol.display;

import java.awt.image.BufferedImage;

import com.madgag.gif.fmsware.AnimatedGifEncoder;

public class GifOutputer implements FrameOutputer {

	private AnimatedGifEncoder encoder = null; 
	
	public GifOutputer(String fname) {
		encoder = new AnimatedGifEncoder();
		encoder.start(fname);
		encoder.setDelay(500); // parameterize later
		encoder.setRepeat(0);

		
	}
	
	@Override
	public void saveFrame(BufferedImage b) {
		encoder.addFrame(b);
	}

	@Override
	public void commit() {
		encoder.finish();
	}

}
