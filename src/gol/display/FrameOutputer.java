package gol.display;

import java.awt.image.BufferedImage;

public interface FrameOutputer {
	
	/**
	 * Save one frame of GOL to the Outputer. If the outputer is an active display this may
	 * change the state of the display
	 * @param b BufferedImage that represents one frame of the GOL
	 */
	public void saveFrame(BufferedImage b);
	
	/**
	 * Create a final "image" of all of the game states have been saved to the Outputer.
	 * For active(screens/terminals) displays this will return the display object
	 */
	public void commit();

}
