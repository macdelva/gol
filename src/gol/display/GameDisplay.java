package gol.display;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import gol.base.GameCell;
import gol.base.GameGrid;

public class GameDisplay <G extends GameGrid, D extends FrameOutputer> {
	
	private G gameGrid = null;
	private D displayer = null;
	
	public G getGameGrid() {
		return gameGrid;
	}

	public void setGameGrid(G gameGrid) {
		this.gameGrid = gameGrid;
	}

	public D getDisplayer() {
		return displayer;
	}

	public void setDisplayer(D displayer) {
		this.displayer = displayer;
	}

	public Color getGridColor() {
		return gridColor;
	}

	public void setGridColor(Color gridColor) {
		this.gridColor = gridColor;
	}

	public Color getLiveCellColor() {
		return liveCellColor;
	}

	public void setLiveCellColor(Color liveCellColor) {
		this.liveCellColor = liveCellColor;
	}

	public Color getDeadCellColor() {
		return deadCellColor;
	}

	public void setDeadCellColor(Color deadCellColor) {
		this.deadCellColor = deadCellColor;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public int getCellSize() {
		return cellSize;
	}

	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}

	public BufferedImage getFrame() {
		return frame;
	}

	public void setFrame(BufferedImage frame) {
		this.frame = frame;
	}

	private Color gridColor = Color.GRAY;
	private Color liveCellColor = Color.BLUE;
	private Color deadCellColor = Color.WHITE;
	private Color backgroundColor = Color.WHITE;
	private int cellSize = 25;
	
	
	private BufferedImage frame = null;

	
	public GameDisplay(G ggrid, D display) {
		gameGrid = ggrid;
		displayer = display;
	}
	
	public GameDisplay(G ggrid, D display, int tileSize) {
		cellSize = tileSize;
		gameGrid = ggrid;
		displayer = display;
		
	}
	
	private void initializeGraphics() {
		int sx,sy,ix,iy;
		sx = gameGrid.getDimensions().x * cellSize;
		sy = gameGrid.getDimensions().y * cellSize;
		ix = gameGrid.getDimensions().x;
		iy = gameGrid.getDimensions().y;
		
		if (frame == null) {
			frame =  new BufferedImage(sx , sy, BufferedImage.TYPE_INT_RGB);
		}
		else {
			frame.getGraphics().clearRect(0, 0, sx , sy);
		}
		Graphics2D pic = (Graphics2D) frame.getGraphics();
		pic.setBackground(backgroundColor);
		
		//draw border and grid
		pic.drawRect(0, 0, sx, sy);
		pic.setColor(this.gridColor);
		for (int x = 0; x < ix; x++) {
			pic.drawLine(x * cellSize, 0, x * cellSize, sy);
		}
		
		for (int y = 0; y < iy; y++) {
			pic.drawLine(0, y * cellSize, sx, y * cellSize);
		}

	}
	/**
	 * Run one turn of the game and save output from the previous turn
	 * @return true if the game state isn't a cycle and so the game was able to advance
	 */
	public boolean tick() {
		
		/*
		 * go through one cycle and save the output
		 */
		
		if (!gameGrid.hasCycled()) {
			updateImage();
			displayer.saveFrame(frame);
			gameGrid.tick();
			return true;
		}
		return false;
	}

	private void updateImage() {
		
		initializeGraphics();
		
		Graphics2D pic = (Graphics2D) frame.getGraphics();
		pic.setColor(this.liveCellColor);
		
		for (GameCell gc : gameGrid.getBoardCells()) {			
			if (gc.isLiving()) {
				int gx = gc.x  * cellSize;
				int gy = gc.y  * cellSize;
				pic.fillRect(gx, gy, cellSize - 1, cellSize - 1);
			}
		}
		
	}
	

}
