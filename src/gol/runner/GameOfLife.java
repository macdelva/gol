package gol.runner;

import java.awt.Color;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import gol.base.FullGameGrid;
import gol.base.GameCell;
import gol.base.GameGrid;
import gol.base.GridInitializer;
import gol.base.SparseGameGrid;
import gol.display.GameDisplay;
import gol.display.GifOutputer;

public class GameOfLife {

	
	static List<GameCell> seed = Arrays.asList(
			new GameCell(1,2), new GameCell(2,2), new GameCell(3,2));
	static List<GameCell> pulsarSeed = Arrays.asList(
			
			);
	
	public static void testSimple() {
		
		GameGrid gg = new SparseGameGrid();
		gg.initialize(6, 6);
		gg.print();
		System.out.println("----------");
		
		gg.seed(seed);
		gg.print();
		System.out.println("----------");
		
		gg.tick();
		gg.print();

		System.out.println("----------");
		
		gg.tick();
		gg.print();
	}
	
	public static void testCycle() {
		GameGrid gg = new FullGameGrid();
		gg.initialize(6, 6);
		gg.seed(seed);
		
		while(!gg.hasCycled()) {
			gg.print();
			gg.tick();
			System.out.println("----------");
		}
		
	}
	public static void testGif() {
		GameGrid gg = new SparseGameGrid();
		gg.initialize(6, 6);
		gg.seed(seed);
		
		GifOutputer gf = new GifOutputer("./test.gif");
		GameDisplay<SparseGameGrid, GifOutputer> gd = new GameDisplay<SparseGameGrid, GifOutputer>((SparseGameGrid)gg, gf);
		gd.setLiveCellColor(Color.RED);
		
		
		gd.tick();
		gd.tick();
		gf.commit();
		
		
	}
	
	public static void testGif2() throws IOException {
		GameGrid gg = new SparseGameGrid();
		
		GridInitializer gi  = new GridInitializer();
		gi.createGrid("testPattern.txt",gg);	
		
		GifOutputer gif = new GifOutputer("./test2.gif");
		GameDisplay<SparseGameGrid, GifOutputer> gd = new GameDisplay<SparseGameGrid, GifOutputer>((SparseGameGrid)gg, gif);
		gd.setLiveCellColor(Color.GREEN);
		gd.setCellSize(8);
		
		while (gd.tick());
		gif.commit();
		
		
	}
	
	public static void testCycleLoad() throws IOException {
		GameGrid gg = new FullGameGrid();
		
		GridInitializer gi  = new GridInitializer();
		gi.createGrid("testPattern.txt",gg);	
		
		while(!gg.hasCycled()) {
			gg.print();
			gg.tick();
			System.out.println("----------");
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		//testGif();
		//testCycleLoad();
		 testGif2();
	}
}
